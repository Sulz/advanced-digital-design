library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

-- Top level entity
entity BCD_counter is
  Port (
      CLOCK_50 : in std_logic;
      Rst : in std_logic;
      HEX0 : out std_logic_vector(6 downto 0);
      HEX1 : out std_logic_vector(6 downto 0);
      HEX2 : out std_logic_vector(6 downto 0)
      );
end BCD_counter;
	
architecture behav of BCD_counter is
	signal count_val_unit : std_logic_vector(3 downto 0);
	signal count_val_ten : std_logic_vector(3 downto 0);
	signal count_val_hun : std_logic_vector(3 downto 0);
	signal nxt_BCD : std_logic_vector(3 downto 0);
	signal custom_clk : std_logic;
	
begin
custom_clk <= not custom_clk after 1000000ns;
process (CLOCK_50, Rst)
begin
	if Rst = '1' then
		count_val_unit <= "0000";
		count_val_ten <= "0000";
		count_val_hun <= "0000";
		
	elsif (rising_edge(CLOCK_50)) then
		if custom_clk = '0' then
			if count_val_unit = "1001" then
				count_val_unit <= "0000";
				count_val_ten <= count_val_ten + 1;
				
				if count_val_ten = "1001" then
					count_val_ten <= "0000";
					count_val_hun <= count_val_hun + 1;
					
					if count_val_hun = "1001" then
						count_val_hun <= "0000";
					end if;
				end if;
				
			else
				count_val_unit <= count_val_unit + 1;
			end if;
		end if;
	end if;

	if count_val_unit = "0001" then
		HEX0 <= "1111001";
	end if;
	if count_val_unit = "0010" then
		HEX0 <= "0100100";
	end if;
	if count_val_unit = "0011" then
		HEX0 <= "0110000";
	end if;
	if count_val_unit = "0100" then
		HEX0 <= "0011000";
	end if;
	if count_val_unit = "0101" then
		HEX0 <= "0010010";
	end if;
	if count_val_unit = "0110" then
		HEX0 <= "0000010";
	end if;
	if count_val_unit = "0111" then
		HEX0 <= "1111000";
	end if;
	if count_val_unit = "1000" then
		HEX0 <= "0000000";
	end if;
	if count_val_unit = "1001" then
		HEX0 <= "0010000";
	end if;
	
	if count_val_unit = "0001" then
		HEX1 <= "1111001";
	end if;
	if count_val_unit = "0010" then
		HEX1 <= "0100100";
	end if;
	if count_val_unit = "0011" then
		HEX1 <= "0110000";
	end if;
	if count_val_unit = "0100" then
		HEX1 <= "0011000";
	end if;
	if count_val_unit = "0101" then
		HEX1 <= "0010010";
	end if;
	if count_val_unit = "0110" then
		HEX1 <= "0000010";
	end if;
	if count_val_unit = "0111" then
		HEX1 <= "1111000";
	end if;
	if count_val_unit = "1000" then
		HEX1 <= "0000000";
	end if;
	if count_val_unit = "1001" then
		HEX1 <= "0010000";
	end if;

	if count_val_unit = "0001" then
		HEX2 <= "1111001";
	end if;
	if count_val_unit = "0010" then
		HEX2 <= "0100100";
	end if;
	if count_val_unit = "0011" then
		HEX2 <= "0110000";
	end if;
	if count_val_unit = "0100" then
		HEX2 <= "0011000";
	end if;
	if count_val_unit = "0101" then
		HEX2 <= "0010010";
	end if;
	if count_val_unit = "0110" then
		HEX2 <= "0000010";
	end if;
	if count_val_unit = "0111" then
		HEX2 <= "1111000";
	end if;
	if count_val_unit = "1000" then
		HEX2 <= "0000000";
	end if;
	if count_val_unit = "1001" then
		HEX2 <= "0010000";
	end if;


end process;
-- 7Seg0 <= count_val_unit;
-- 7Seg1 <= count_val_ten;
-- 7Seg2 <= count_val_hun;
--Count_unit <= count_val_unit;
--Count_ten <= count_val_ten;
--Count_hun <= count_val_hun;
end behav;

-- 7Seg mappings:
--0001 -> 11111001
--0010 -> 10100100
--0011 -> 10110000
--0100 -> 10011000
--0101 -> 10010010
--0110 -> 10000010
--0111 -> 11111000
--1000 -> 10000000
--1001 -> 10010000

