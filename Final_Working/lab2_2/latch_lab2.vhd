Library ieee;
USE ieee.std_logic_1164.all;

ENTITY latch_lab2 IS
PORT (D, Clk : IN STD_LOGIC;
	Q0, Q1, Q2 : OUT STD_LOGIC);
END latch_lab2;

ARCHITECTURE Behavior of latch_lab2 is
begin
	process (D, Clk)
	begin
		if ( rising_edge(Clk) ) then
			Q0 <= D;
		elsif (falling_edge (clk)) then
			Q1 <= D;
		elsif (Clk = '1') then
			Q2 <= D;
		end if;
	end process;
end Behavior;
