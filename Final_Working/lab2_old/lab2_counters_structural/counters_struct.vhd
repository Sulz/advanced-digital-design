library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries


-- Top level entity
entity counters_struct is
	generic(count_len : integer := 16);
  Port (En : in std_logic;
      Clk : in std_logic;
      Rst : in std_logic;
      Count: out std_logic_vector(count_len downto 0));
end counters_struct;
	

-- Definition of interals of top level entity
architecture structure of counters_struct is

-- It has a flip flop component. With these mappings.
component T_Flop is
 generic(count_len : integer);
  port (en, clk, rst : in std_logic;
      count : out std_logic_vector (count_len downto 0));
 end component T_Flop;

-- Each T_Flop output is a signal as it is not visible from outside the entity
 -- NOT NEEDED!!
-- signal op : std_logic_vector(15 downto 0);

-- Port mapping the T_Flop component

begin
  flop_0: T_Flop generic map(count_len => count_len)
    port map(
      en => En,
      clk => Clk,
      rst => Rst,
      count => Count
    );

end architecture;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries


-- Define the T_Flop component as an entity

-- Having already declared this component with these ports. 
--     Now instantiating/defining the actual component.
entity T_Flop is
generic(count_len : integer);
 port (en, clk, rst : in std_logic;
      count : out std_logic_vector (count_len downto 0));
end entity;


-- Define architecture of the entity
architecture flop_behav of T_Flop is
  component T_Flop_comp 
	port(
          en, clk, rst : in std_logic;
          op : out std_logic
        );
  end component;

signal op_vector, en_vector : std_logic_vector (count_len + 1 downto 0) := (others => '0');
	
begin
  T_flop_part: for i in 0 to count_len generate
    actual_flop: T_Flop_comp port map(
          en => en_vector(i),
          clk => clk,
          rst => rst,
          op => op_vector(i));
          en_vector(i + 1) <= op_vector(i) and en_vector(i);
          count(count_len downto 0) <= op_vector(count_len downto 0);
          en_vector(0) <= en;
    end generate;
end architecture;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

entity T_Flop_comp is
  port (en, clk, rst : in std_logic;
        op : out std_logic);
end entity;

architecture arch of T_Flop_comp is

signal not_op : std_logic := '1';

begin
  process (clk, rst, en)
  begin
    if rst = '0' then
      op <= '0';
      not_op <= '1';
	else
		if rising_edge(clk) and en = '1' then
			op <= not_op;
			not_op <= not not_op;
		end if;
	end if;
  end process;
end architecture ; -- arch