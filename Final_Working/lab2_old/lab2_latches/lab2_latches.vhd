Library ieee;
USE ieee.std_logic_1164.all;

ENTITY lab2_latches IS
	PORT (D, Clk : IN STD_LOGIC;
		Q0, Q1, Q2 : OUT STD_LOGIC);
END lab2_latches;

ARCHITECTURE Behavior of lab2_latches is
begin
	process (D, Clk)
	begin
		if ( rising_edge(Clk) ) then
			Q0 <= D;
		-- else Q0 <= 'Z';
		end if;
		
		if (falling_edge (clk)) then
			Q1 <= D;
		-- else Q1 <= 'Z';
		end if;
		
		if (Clk = '1') then
			Q2 <= D;
		else Q2 <= '0';
		end if;
		
	end process;
end Behavior;
