
library ieee;
use ieee.std_logic_1164.all;

entity lab2_chris is
	port(SW : in std_logic_vector (9 downto 0);
		HEX0, HEX1, HEX2 : out std_logic_vector (0 to 6);
		CLOCK_50 : in std_logic;
		KEY : in std_logic_vector (3 downto 0);
		testled : out std_logic);
end entity;

architecture arch of lab2_chris is

component ff_counter
	generic(counter_length : integer := 3);
	port(enable, clk, clear_n : in std_logic;
		q : out std_logic_vector (counter_length downto 0));
end component;

COMPONENT bcd_decoder
	PORT ( bcd : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		Display : OUT STD_LOGIC_VECTOR(0 TO 6));
END COMPONENT;

COMPONENT scale_clock
	port (
		clkin : in std_logic;
		clkout : out std_logic
	);
END COMPONENT;

signal q_out0, q_out1, q_out2 : std_logic_vector (3 downto 0);
signal c1_en, c2_en : std_logic;
signal slow_clk : std_logic;
 begin
	
	clk0 : scale_clock port map(CLOCK_50, slow_clk);
	
	c0 : ff_counter port map(enable => SW(0),
				 clear_n => KEY(3),
				 clk => slow_clk,
				 q => q_out0);
				 
	c1 : ff_counter port map(enable => SW(0),
				 clear_n => KEY(3),
				 clk => c1_en,
				 q => q_out1);
	
	c2 : ff_counter port map(enable => SW(0),
				clear_n => KEY(3),
				clk => c2_en,
				q => q_out2);
	
	h0 : bcd_decoder port map(q_out0, HEX0);
	h1 : bcd_decoder port map(q_out1, HEX1);
	h2 :  bcd_decoder port map(q_out2, HEX2);
			
	process (q_out0, slow_clk)
	begin
		if(rising_edge(slow_clk)) then
			if(q_out0 = "1001") then
				c1_en <= '1';
			else
				c1_en <= '0';
			end if;
		end if;
		
	end process;	
	
	process (q_out1, c1_en)
	begin	
		if(rising_edge(c1_en)) then
			if(q_out1 = "1001") then
				c2_en <= '1';
			else
				c2_en <= '0';
			end if;
		end if;		
	end process;
	
	testled <= KEY(3);
	
end arch;
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity bcd_decoder is
	PORT ( bcd : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		Display : OUT STD_LOGIC_VECTOR(0 TO 6));
end entity;

architecture arch of bcd_decoder is
BEGIN
	process ( bcd ) 
	begin
		if ( bcd = "0000" ) then
			Display <= "0000001";	-- 0
		Elsif ( bcd = "0001" ) then
			Display <= "1001111";	-- 1
		Elsif ( bcd = "0010" ) then
			Display <= "0010010";	-- 2
		Elsif ( bcd = "0011" ) then
			Display <= "0000110";	-- 3
		Elsif ( bcd = "0100" ) then
			Display <= "1001100";	-- 4
		Elsif ( bcd = "0101" ) then
			Display <= "0100100";	-- 5
		Elsif ( bcd = "0110" ) then
			Display <= "0100000";	-- 6
		Elsif ( bcd = "0111" ) then
			Display <= "0001111";	-- 7
		Elsif ( bcd = "1000" ) then
			Display <= "0000000";	-- 8
		Elsif ( bcd = "1001" ) then
			Display <= "0001100";	-- 9
		Else
			Display <= "1111111";	-- blank
		end if;
	end process;
end arch;
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity ff_counter is
	generic(counter_length : integer := 3);
	port(enable, clk, clear_n : in std_logic;
		q : out std_logic_vector (counter_length downto 0));
end entity;

architecture arch of ff_counter is

component t_type_ff port(enable, clk, clear_n : in std_logic;
	q : out std_logic := '0');
end component;

signal q_vector, enable_vector : std_logic_vector (counter_length+1 downto 0) := (others => '0');
signal nclear_vector : std_logic_vector (counter_length downto 0) := (others => '0');
begin
	process (q_vector, clear_n)
	begin
		if (q_vector (counter_length downto 0) = "1010") then
			nclear_vector <= "0000";
		else
			nclear_vector <= "1111";
		end if;
		
		if (clear_n = '0') then
			nclear_vector <= "0000";
		end if;
	end process;
	t_ff: for i in 0 to counter_length generate
		tx : t_type_ff port map(enable => enable_vector(i),
								clk => clk,
								clear_n => nclear_vector(i),
								q => q_vector(i));
		enable_vector(i+1) <= q_vector(i) and enable_vector(i);
		q <= q_vector (counter_length downto 0);
		enable_vector(0) <= enable;
	end generate;
end architecture;


library ieee;
use ieee.std_logic_1164.all;

entity t_type_ff is
port(enable, clk, clear_n : in std_logic;
	q : out std_logic := '0');
end entity;

architecture arch of t_type_ff is
signal q_n : std_logic := '1';
begin
	process(clk, clear_n)
	begin
		if clear_n = '0' then
			q <= '0';
			q_n <= '1';
		else
			if rising_edge(clk) and enable = '1' then
				q <= q_n;
				q_n <= not q_n;
			end if;
		end if;
	end process;
end architecture;
---------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
------------------------------------------
entity scale_clock is
	port (
		clkin : in std_logic;
		clkout : out std_logic
	);
end scale_clock;
------------------------------------------
architecture behaviour of scale_clock is
	
	constant CLK_FREQ : integer := 10000000;
	constant BLINK_FREQ : integer := 1;
	constant CNT_MAX : integer := CLK_FREQ/BLINK_FREQ/2-1;
	signal cnt : unsigned(24 downto 0);
	signal blink : std_logic;

begin
		process(clkin)
		begin
			if rising_edge(clkin) then
			if cnt=CNT_MAX then
			cnt <= (others => '0');
			blink <= not blink;
			else
			cnt <= cnt + 1;
			end if;
			end if;
		end process;
	clkout <= blink;
end behaviour;
------------------------------------------