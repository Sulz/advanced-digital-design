library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

entity counters_struct is
  Port (  rst : in std_logic;
      clk : in std_logic;
      count: out std_logic_vector(15 downto 0));
end counters_struct;

architecture behav of counters_struct is

component T-Flop
  port (Rst, Clk : in std_logic;
      count : out std_logic);

component 

end component

begin
  TF1: T-Flop port map (
      Rst,
      Clk,
      Count);
      
  TF2: counters_struct port map (
      Rst,
      Clk,
      Count);

  TF3: counters_struct port map (
      Rst,
      Clk,
      Count);

  TF4: counters_struct port map (
      Rst,
      Clk,
      Count);

end behav


architecture 