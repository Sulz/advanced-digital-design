library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries
use ieee.numeric_std.all;

entity proj_w_mif is
	Port(
		--Sulien
		clk : in std_logic;
		fire : in std_logic;
		
		rows : out std_logic;
		
		--x_coordinate : in std_logic_vector(2 downto 0);
		--y_coordinate : in std_logic_vector(2 downto 0);
		
		-- Damiens Code
		--		Inputs
		SW : in std_logic_vector (9 downto 0);
		--KEY : in std_logic_vector (3 downto 0);
		
		HEX0 : out std_logic_vector (6 downto 0);
		HEX1 : out std_logic_vector (6 downto 0);
		HEX2 : out std_logic_vector (6 downto 0);
		HEX3 : out std_logic_vector (6 downto 0);
		
		LEDG : out std_logic_vector(7 downto 0);		
		
		random_var : out std_logic;
		address_tester : out std_logic_vector(5 downto 0);
		bit_tester : out std_logic
		
	);
end proj_w_mif;

architecture behav of proj_w_mif is

	--Suliens shizz
component incrementer is
	port(
			inc_clk : in std_logic;
			game_over : in std_logic;
			output : out std_logic_vector (5 downto 0);
			loaded : out std_logic
		);
end component incrementer;

component mega_mif is
	port(
			address : in std_logic_vector(5 downto 0);
			clock : in std_logic := '1';
			q : out std_logic 
		);
end component mega_mif;

--Damiens Components
component char_7seg
	port( 	
		HEX : in std_logic_vector (3 downto 0);
		Display : out std_logic_vector (6 downto 0)
		);
end component;

 -- Not currently used
component Hit_LED is    
	Port (                   
		LEDR : out  STD_LOGIC:='0'
		);			
end component;
component Shot_LED is    
	Port (                   
		LEDG : out  STD_LOGIC:='0'
		);			
end component;
	
	--Suliens' Code
	type game_status is array (63 downto 0) of std_logic;
	signal game_state : game_status;
	signal addr : std_logic_vector(5 downto 0);
	signal this_bit : std_logic;
	signal pointer : std_logic_vector(5 downto 0);
	signal add_ptr : std_logic_vector(5 downto 0);
	
	signal num_hits : integer := 0;
	
	-- From Mays Code
	signal intX,intY : integer; -- for the conversion
	signal hit : std_logic;
	signal index : integer;
	
	--signal i, j : integer;

	--Damiens Code
	signal hit_count_ones : std_logic_vector (3 downto 0) := "0000";
	signal hit_count_tens : std_logic_vector (3 downto 0) := "0000";
	signal shot_count_ones : std_logic_vector (3 downto 0) := "0000";
	signal shot_count_tens : std_logic_vector (3 downto 0) := "0000";

	signal has_loaded : std_logic;
	signal LED_flash_miss : std_logic;
	signal index_update : integer;
	signal hit_sig : std_logic;

	signal end_this : std_logic := '0';
	
	
begin
adresser : incrementer
	port map(
			inc_clk => clk,
			game_over => end_this,
			output => pointer,
			loaded => has_loaded
		);
--std_logic_vector(to_unsigned(input_1, output_1a'length));

add_ptr <= pointer;

mif: mega_mif
	port map(
			address => add_ptr,
			clock => clk,
			q  => this_bit	
		);
	
intX <= conv_integer(SW(2 downto 0)); --conversion from std_logic_vector to integer
intY <= conv_integer(SW(5 downto 3));

index <= (8 * intY + intX);

process(add_ptr, hit_sig)
begin
	if (has_loaded = '0') then
		game_state(conv_integer(pointer)) <= this_bit;
		address_tester <= pointer;
		bit_tester <= this_bit;
	elsif (has_loaded = '1' and hit_sig = '1') then
		game_state(index_update) <= '0';
	end if;
	
end process;


process (hit_sig)
begin
	if hit_sig = '1' then
		num_hits <= num_hits + 1;
		end_this <= '0';
	end if;
	
	if num_hits = 12 then
		--LEDG <= "00000000";
		end_this <= '1';
	end if;
	
end process;
		
 --Mays Code
 -- Updated to get the single index

 --Damiens
	s0 : char_7seg port map (HEX => hit_count_ones, Display => HEX0);	
	s1 : char_7seg port map (HEX => hit_count_tens, Display => HEX1);
--		s4 : Hit_LED port map (LEDR => hit_led_count);*
	s2 : char_7seg port map (HEX => shot_count_ones, Display => HEX2);
	s3 : char_7seg port map (HEX => shot_count_tens, Display => HEX3);	
--		s5 : Shot_LED port map (LEDG => shot_led_count);
 
process(fire)
	begin
	
	if (falling_edge(fire) and has_loaded = '1') then
		if game_state(index) = '1' then -- checking the coordinates in the board
			hit <= '1'; -- send a high for a hit	
			hit_sig <= '1';
			hit <= '0';
			LED_flash_miss <= '0';
			index_update <= index;		
		else
			hit <= '0'; -- no hit
			hit_sig <= '0';
			LED_flash_miss <= '1';
	  	end if;
	end if;
	random_var <= hit_sig;

	if SW(9 downto 8)= "01" OR SW(9 downto 8)="11" then
			
			--Reset game, including board
		hit_count_ones <= "0000"; 
		hit_count_tens <= "0000";
		shot_count_ones <= "0000"; 
		shot_count_tens <= "0000";
		--has_loaded <= '0';
			
	elsif (SW(9 downto 8) = "00" and has_loaded = '1') then
		if rising_edge(fire) then -----------clock1
			if hit_sig = '1' then
				LEDG <= "00000000";
				hit_count_ones <= hit_count_ones + 1;
				if hit_count_ones = "1001" then
					hit_count_ones <= "0000";
					hit_count_tens <= hit_count_tens +1;
					if hit_count_tens = "1001" then
						hit_count_tens <= "0000";
					end if;
				end if;
				
				shot_count_ones <= shot_count_ones + 1;
				if shot_count_ones = "1001" then
					shot_count_ones <= "0000";
					shot_count_tens <= shot_count_tens +1;
					if shot_count_tens = "1001" then
						shot_count_tens <= "0000";
					end if;
				end if;

			elsif hit_sig = '0' then	
				LEDG <= "11111111";
				shot_count_ones <= shot_count_ones + 1;	
				if shot_count_ones = "1001" then
					shot_count_ones <= "0000";
					shot_count_tens <= shot_count_tens +1;
					if shot_count_tens = "1001" then
						shot_count_tens <= "0000";
					end if;
				end if;	
			end if;				
		end if;
	end if;	
end process;

end architecture;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

entity incrementer is
	port(
			inc_clk : in std_logic;
			game_over : in std_logic;
			output : out std_logic_vector(5 downto 0);
			loaded : out std_logic
	);
end entity incrementer;



architecture behav of incrementer is
	signal address : std_logic_vector(5 downto 0);

begin
process (inc_clk)
	begin
		if rising_edge(inc_clk) then
			if game_over = '0' then
				if address = "111111" then
					address <= address;
					loaded <= '1';
				else
					address <= address + 1;
					loaded <= '0';
				end if;
			end if;
		
			if game_over = '1' then
				address <= "000000";
				loaded <= '0';
			end if;
			
		end if;
end process;

output <= address;

end architecture behav;


LIBRARY ieee;
USE ieee.std_logic_1164.all;

LIBRARY altera_mf;
USE altera_mf.all;

ENTITY mega_mif IS
	PORT
	(
		address		: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		q			: OUT STD_LOGIC
	);
END mega_mif;


ARCHITECTURE SYN OF mega_mif IS

	SIGNAL sub_wire0	: STD_LOGIC;

	COMPONENT altsyncram
	GENERIC (
		clock_enable_input_a		: STRING;
		clock_enable_output_a		: STRING;
		init_file		: STRING;
		intended_device_family		: STRING;
		lpm_hint		: STRING;
		lpm_type		: STRING;
		numwords_a		: NATURAL;
		operation_mode		: STRING;
		outdata_aclr_a		: STRING;
		outdata_reg_a		: STRING;
		widthad_a		: NATURAL;
		width_a		: NATURAL;
		width_byteena_a		: NATURAL
	);
	PORT (
			clock0	: IN STD_LOGIC ;
			address_a	: IN STD_LOGIC_VECTOR (5 DOWNTO 0);
			q_a	: OUT STD_LOGIC
	);
	END COMPONENT;

BEGIN
	q    <= sub_wire0;

	altsyncram_component : altsyncram
	GENERIC MAP (
		clock_enable_input_a => "BYPASS",
		clock_enable_output_a => "BYPASS",
		init_file => "proj_w_mif.mif",
		intended_device_family => "Cyclone II",
		lpm_hint => "ENABLE_RUNTIME_MOD=NO",
		lpm_type => "altsyncram",
		numwords_a => 64,
		operation_mode => "ROM",
		outdata_aclr_a => "NONE",
		outdata_reg_a => "CLOCK0",
		widthad_a => 6,
		width_a => 1,
		width_byteena_a => 1
	)
	
	PORT MAP (
		clock0 => clock,
		address_a => address,
		q_a => sub_wire0
	);

END SYN;

-- char_7seg ------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity char_7seg is 
	port( 	
		HEX : in std_logic_vector (3 downto 0); 
		Display : out std_logic_vector (6 downto 0)
		);
end entity;

architecture behaviour of char_7seg is
	begin
		Process (HEX)
			begin
				case HEX is
					when "0000" => Display <= "1000000";--0
					when "0001" => Display <= "1111001";--1
					when "0010" => Display <= "0100100";--2
					when "0011" => Display <= "0110000";--3
					when "0100" => Display <= "0011001";--4
					when "0101" => Display <= "0010010";--5
					when "0110" => Display <= "0000010";--6
					when "0111" => Display <= "1111000";--7
					when "1000" => Display <= "0000000";--8
					when "1001" => Display <= "0010000";--9
					when others => Display <= "0000000";--0
				end case;		
		end Process;
end architecture;
-----------------------------------------------------

---- LEDR ---
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
entity Hit_LED is    
	Port (                   
		LEDR : out  STD_LOGIC:='0'
		);
		
end Hit_LED;
 
architecture Behavioral of Hit_LED is
	begin
 
		LEDR <= '1';                   
 
end Behavioral;

---- LEDG --------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
entity Shot_LED is    
	Port (                   
		LEDG : out  STD_LOGIC:='0'
		);
		
	end Shot_LED;
 
architecture Behavioral of Shot_LED is
	begin
 
		LEDG <= '1';                   
 
end Behavioral;

---- End Of LED's ----------------------