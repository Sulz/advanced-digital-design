-----------------------------------------------
--    Advanced Digital Design Project    --
--          Battle-Ship        --
-----------------------------------------------
-----------------------------------------------
--  Sulien Rigby  S3436413         --
--  Le-May Vu   S3433867         --
--  Damien Holt   S9407685         --
-----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

entity proj_w_mif is
  Port(
    -- Internal Clock
    clk : in std_logic;
    
    --Button press for firing at target coordinate.
    fire : in std_logic;

    --Switch inputs for coordinates and reset.
    SW : in std_logic_vector (9 downto 0);
    
    -- The 4 7-Set displays
    HEX0 : out std_logic_vector (6 downto 0);
    HEX1 : out std_logic_vector (6 downto 0);
    HEX2 : out std_logic_vector (6 downto 0);
    HEX3 : out std_logic_vector (6 downto 0);
    
    -- All the green LED's
    LEDG : out std_logic_vector(7 downto 0);   
    
    -- All the red LED's
    LEDR : out std_logic_vector(9 downto 0);  
     
    -- Debug. Shows when a shot was a hit.
    random_var : out std_logic; 
    
    --Debug. Shows address given by incrementer module for attaining data at that address in MIF.
    address_tester : out std_logic_vector(5 downto 0);
    
    -- Debug. When loading bits from MIF. This mirrors the bit being loaded.
    bit_tester : out std_logic
    );
    
end proj_w_mif;

architecture behav of proj_w_mif is

-----------------------------------------------
--        Incrementer          --
-----------------------------------------------
-- Iterates from 0-63. Giving address for MIF.
--    inc_clk : Clock for the component.
--    output : Next address.
--    loaded : When game board is loaded. Commence game.
--    game_over : When time to start new game. 
--          (Start loading from 0)

component incrementer is
  port(
      inc_clk : in std_logic;
      game_over : in std_logic;
      output : out std_logic_vector (5 downto 0);
      loaded : out std_logic
    );
end component incrementer;

-----------------------------------------------
--      Memory Initialization      --
-----------------------------------------------
--    address : Where to get next bit of data from
--    clock : MIF clock.
--    q : the bit at address 'address'
component mega_mif is
  port(
      address : in std_logic_vector(5 downto 0);
      clock : in std_logic := '1';
      q : out std_logic 
    );
end component mega_mif;

component char_7seg
  port(   
    HEX : in std_logic_vector (3 downto 0);
    Display : out std_logic_vector (6 downto 0)
    );
end component;

component Hit_LED is    
  Port (                   
    LEDR : out  STD_LOGIC:='0'
    );      
end component;

component Shot_LED is    
  Port (                   
    LEDG : out  STD_LOGIC:='0'
    );      
end component;
  
  -- The game state is an array of bits. '1' indicates presence of ship.
  type game_status is array (63 downto 0) of std_logic;
  
  -- The current game board. '1' indicates presence of ship.
  signal game_state : game_status;
  
  -- The bit from the MIF to place in array
  signal this_bit : std_logic;
  
  -- The output from the incrementer.
  signal add_ptr : std_logic_vector(5 downto 0);
  
  -- The pointer to pass to the MIF.
  signal pointer : std_logic_vector(5 downto 0);
  
  --signal num_hits : integer := 0;
  signal num_hits : std_logic_vector(3 downto 0);
  
  -- For converting the vector array of switches into coordinates.
  signal intX,intY : integer;
  
  -- To recognize a hit
  signal hit : std_logic;
  
  -- Convert the coordinates to an integer index.
  signal index : integer;
  
 -- For keeping track of the number of shots, hits, and for passing to HEX.
  signal hit_count_ones : std_logic_vector (3 downto 0) := "0000";
  signal hit_count_tens : std_logic_vector (3 downto 0) := "0000";
  signal shot_count_ones : std_logic_vector (3 downto 0) := "0000";
  signal shot_count_tens : std_logic_vector (3 downto 0) := "0000";

  -- To recognize when the gamestate has finished loading.
  signal has_loaded : std_logic;
  
  -- Debug - tracing LED's.
  signal LED_flash_miss : std_logic;
  
  -- Ingame array index used for updating the board on a hit.
  signal index_update : integer;
  
  -- Another signal for passing around on hit (perhaps replace with 'hit')
  signal hit_sig : std_logic;
    
  -- Reload the game when all the ships have been hit.
  signal end_this : std_logic;
  

-- Port map the incrementer
begin
adresser : incrementer
  port map(
      inc_clk => clk,
      game_over => end_this,
      output => pointer,
      loaded => has_loaded
    );

add_ptr <= pointer;

-- Port map the memory initialization file
mif: mega_mif
  port map(
      address => add_ptr,
      clock => clk,
      q  => this_bit  
    );

--converting coordinates from std_logic_vector to integer
intX <= conv_integer(SW(2 downto 0)); 
intY <= conv_integer(SW(5 downto 3));

-- Converting from coordinates into an index.
index <= (8 * intY) + intX;
    
process(clk, add_ptr, has_loaded, hit_sig)
begin
  -- If the game has not loaded, keep loading.
  if (has_loaded = '0') then
    -- Put the recently attained bit from MIF into gamestate array.
    game_state(conv_integer(pointer)) <= this_bit;
    
    -- Following are \ statements for tracing the address and the bit at that address.
    address_tester <= pointer;
    bit_tester <= this_bit;
  
  -- If game has finished loading, and user scored a hit.
  elsif (has_loaded = '1' and hit_sig = '1') then
    -- Set the appropriate coordinate/index in the game to 0.
    game_state(index_update) <= '0';
  end if;
  
end process;

 --Port map the 7-Segment display components
  s0 : char_7seg port map (HEX => hit_count_ones, Display => HEX0); 
  s1 : char_7seg port map (HEX => hit_count_tens, Display => HEX1);
  s2 : char_7seg port map (HEX => shot_count_ones, Display => HEX2);
  s3 : char_7seg port map (HEX => shot_count_tens, Display => HEX3);
 
process(fire)
  begin
  if (falling_edge(fire) and has_loaded = '1') then
    -- Check if shot at provided coordinate is a hit.
    if game_state(index) = '1' then
        -- send a high for a hit  
        hit <= '1'; 
        -- Tell the system there was a hit.
        hit_sig <= '1';
        -- Reset hit. Required for correct operations.
        hit <= '0';
        -- Whatever the current index is, update that coordinate in the game board.
        index_update <= index;
        -- Debug Statement.
        LED_flash_miss <= '0';
    else
      -- Shot was a miss
        hit <= '0'; 
        hit_sig <= '0';
        --Debug Statement.
        LED_flash_miss <= '1'; 
    end if;
  end if;
  -- Debug Statement. Used for tracing shots that hit.
  random_var <= hit_sig;

  -- Check conditions for new game.
  if SW(9 downto 8)= "01" OR SW(9 downto 8)="11" or has_loaded = '0' then
    -- Reset scores
    hit_count_ones <= "0000"; 
    hit_count_tens <= "0000";
    shot_count_ones <= "0000"; 
    shot_count_tens <= "0000";
    LEDG <= "00000000";
    LEDR <= "0000000000";
    
      
  -- If the game has been loaded, and switches are set for play.
  elsif (SW(9 downto 8) = "00" and has_loaded = '1') then
    if rising_edge(fire) then
      if hit_sig = '1' then
        -- Flash red LED's on hit
        LEDR <= "1111111111";
        LEDG <= "00000000";
        
        -- Update the number of hits so far.
        hit_count_ones <= hit_count_ones + 1;
        if hit_count_ones = "1001" then
          hit_count_ones <= "0000";
          hit_count_tens <= hit_count_tens +1;
          
          if hit_count_tens = "1001" then
            hit_count_tens <= "0000";
          end if;
        end if;
                
        -- Update number of shots so far.
        shot_count_ones <= shot_count_ones + 1;
        if shot_count_ones = "1001" then
          shot_count_ones <= "0000";
          shot_count_tens <= shot_count_tens +1;
          if shot_count_tens = "1001" then
            shot_count_tens <= "0000";
          end if;
        end if;

      -- User shot was a miss.
      elsif hit_sig = '0' then
        -- Flash green LED's on miss
        LEDG <= "11111111";
        LEDR <= "0000000000";
        -- Update number of shots so far.
        shot_count_ones <= shot_count_ones + 1; 
        if shot_count_ones = "1001" then
          shot_count_ones <= "0000";
          shot_count_tens <= shot_count_tens +1;
          if shot_count_tens = "1001" then
            shot_count_tens <= "0000";
          end if;
        end if; 
      end if;       
    end if;
  end if;
  
-- Once the user hits the number of hittable components in the array, signal end of game.
  if (hit_count_ones = "0101" and hit_count_tens = "0000") then
    -- End of game signal
    end_this <= '1';
    -- Reset the hit signal.
    hit_sig <= '0';
  else
    -- Not end of game yet.
    end_this <= '0';
  end if;   
--  
end process;

end architecture;

-----------------------------------------------
--        Incrementer          --
-----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

entity incrementer is
  port(
      inc_clk : in std_logic;
      game_over : in std_logic;
      output : out std_logic_vector(5 downto 0);
      loaded : out std_logic
  );
end entity incrementer;

architecture behav of incrementer is
    -- Address to return
  signal address : std_logic_vector(5 downto 0);
    -- Used for determining whether game requires further loading.
    signal load_check : std_logic := '0';
    
begin
process (inc_clk)
  begin
    if rising_edge(inc_clk) then
      -- Once we've gone through each address.
      if address = "111111" then
        -- Keep address constant
        address <= address;
        -- Game has loaded.
        load_check <= '1';
      end if;
        
      -- If the game has played through.
      if game_over = '1' then
        -- Reset the address
        address <= "000000";
        -- Signal the game hasn't loaded.
        load_check <= '0';
      end if;
      
      -- While the game hasn't loaded (effectively a loop on each clock).
      if load_check = '0' then
        -- Increment the address.
        address <= address + 1;
      end if;
    end if;     
end process;
-- Pass results back to system
output <= address;
loaded <= load_check;
end architecture behav;


-----------------------------------------------
--       Memory Initialization File    --
-----------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.all;
LIBRARY altera_mf;
USE altera_mf.all;

-- Megafunction generated code.
ENTITY mega_mif IS
  PORT
  (
    address   : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
    clock   : IN STD_LOGIC  := '1';
    q     : OUT STD_LOGIC
  );
END mega_mif;


ARCHITECTURE SYN OF mega_mif IS

  SIGNAL sub_wire0  : STD_LOGIC;

  COMPONENT altsyncram
  GENERIC (
    clock_enable_input_a    : STRING;
    clock_enable_output_a   : STRING;
    init_file   : STRING;
    intended_device_family    : STRING;
    lpm_hint    : STRING;
    lpm_type    : STRING;
    numwords_a    : NATURAL;
    operation_mode    : STRING;
    outdata_aclr_a    : STRING;
    outdata_reg_a   : STRING;
    widthad_a   : NATURAL;
    width_a   : NATURAL;
    width_byteena_a   : NATURAL
  );
  PORT (
      clock0  : IN STD_LOGIC ;
      address_a : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
      q_a : OUT STD_LOGIC
  );
  END COMPONENT;

BEGIN
  q    <= sub_wire0;

  altsyncram_component : altsyncram
  GENERIC MAP (
    clock_enable_input_a => "BYPASS",
    clock_enable_output_a => "BYPASS",
    init_file => "proj_w_mif.mif",
    intended_device_family => "Cyclone II",
    lpm_hint => "ENABLE_RUNTIME_MOD=NO",
    lpm_type => "altsyncram",
    numwords_a => 64,
    operation_mode => "ROM",
    outdata_aclr_a => "NONE",
    outdata_reg_a => "CLOCK0",
    widthad_a => 6,
    width_a => 1,
    width_byteena_a => 1
  )
  
  PORT MAP (
    clock0 => clock,
    address_a => address,
    q_a => sub_wire0
  );

END SYN;

-----------------------------------------------
--        7 Segment Displays       --
-----------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

entity char_7seg is 
  port(   
    HEX : in std_logic_vector (3 downto 0); 
    Display : out std_logic_vector (6 downto 0)
    );
end entity;

architecture behaviour of char_7seg is
  begin
    Process (HEX)
      begin
        case HEX is
          -- Depending on the number passed in HEX. Display appropriate decimal on 7Segs.
          when "0000" => Display <= "1000000";--0
          when "0001" => Display <= "1111001";--1
          when "0010" => Display <= "0100100";--2
          when "0011" => Display <= "0110000";--3
          when "0100" => Display <= "0011001";--4
          when "0101" => Display <= "0010010";--5
          when "0110" => Display <= "0000010";--6
          when "0111" => Display <= "1111000";--7
          when "1000" => Display <= "0000000";--8
          when "1001" => Display <= "0010000";--9
          when others => Display <= "0000000";--0
        end case;   
    end Process;
end architecture;

-----------------------------------------------
--        Red LED's          --
-----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
entity Hit_LED is    
  Port (                   
    LEDR : out  STD_LOGIC:='0'
    );
    
end Hit_LED;
 
architecture Behavioral of Hit_LED is
  begin
    LEDR <= '1';                   
end Behavioral;


-----------------------------------------------
--        Green LED's          --
-----------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
entity Shot_LED is    
  Port (                   
    LEDG : out  STD_LOGIC:='0'
    );
  end Shot_LED;
 
architecture Behavioral of Shot_LED is
  begin
    LEDG <= '1';                   
end Behavioral;