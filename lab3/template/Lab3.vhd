library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

entity lab3 is
	Port (  Ai, Bi: in std_logic_vector(7 downto 0);
		   	LdA,LdB, LdS, Ci, clk : in std_logic;
			Co, Ovr: out std_logic;
			S: out std_logic_vector(7 downto 0));
end lab3;


architecture behavioral of lab3 is
	signal TA, TB, TS: std_logic_vector(7 downto 0);--define some intermediate signals
	signal TCo, TOvr: std_logic;	
begin
  --clocked_Stuff:
	process (clk) --clocked events ONLY
	begin
	  if (clk'event and clk='1') then --could use rising_edge function
		if LdA then TA <= Ai; end if;
		--add the rest of the clocked events in here
	   end if;
	end process Clocked_stuff;
--now add in the unclocked stuff here - i.e., the adder function
--this might either be a second process, or a bunch of concurrent statements --your choice

end behavioral;