library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

entity fsm_testbench is
	Port (  Ai, Bi: in std_logic_vector(7 downto 0);
		   	LdA, LdB, LdS, Ci, clk : in std_logic;
			Co, Ovr: out std_logic;
			S: out std_logic_vector(7 downto 0));
end fsm_testbench;


architecture behavioral of fsm_testbench is
	signal TA, TB, TS: std_logic_vector(7 downto 0);--define some intermediate signals
	signal TCo, TOvr: std_logic;	
begin
  --clocked_Stuff:
	process (clk) --clocked events ONLY
	begin
	  if (clk'event and clk='1') then --could use rising_edge function
		if LdA = '1' then TA <= Ai; end if;
		
		--add the rest of the clocked events in here
		if LdB = '1' then TB <= Bi; end if;
		if LdS = '1' then 
			Co <= TCo;
			Ovr <= TOvr;
			S <= TS;
		end if;
	   end if;
	end process Clocked_stuff;

--now add in the unclocked stuff here - i.e., the adder function
--this might either be a second process, or a bunch of concurrent statements --your choice

process (Clk)
	begin
		Ts <= Ai + Bi + Ci;
		--if Ai + Bi + Ci > "11111111" then
		if Ts < Ai then
			TCo <= '1';
			--Co <= (Ai and Bi) OR (Ci and (Ai or Bi));
		else TCo <= '0';
		end if;
		
		if Ai(7) = '1' then
			if Bi(7) = '1' then
				if Ts(7) = '0' then
					TOvr <= '1';
				end if;
			end if;
		else TOvr <= '0';
		end if;
		
end process;

end behavioral;