LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY part3 IS
	PORT ( SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6));
END part3;


ARCHITECTURE Behavior OF part3 IS
COMPONENT mux_2bit_3to1
	PORT ( S, U, V, W : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		M : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
END COMPONENT;

COMPONENT char_7seg
	PORT ( C : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6));
END COMPONENT;

SIGNAL M : STD_LOGIC_VECTOR(1 DOWNTO 0);
BEGIN
	M0: mux_2bit_3to1 PORT MAP (SW(9 DOWNTO 8), SW(5 DOWNTO 4), SW(3 DOWNTO 2),
	SW(1 DOWNTO 0), M);
	H0: char_7seg PORT MAP (M, HEX0);
END Behavior;


LIBRARY ieee;
USE ieee.std_logic_1164.all;

 -- implement a 2-bit wide 3-to-1 multiplexer

ENTITY mux_2bit_3to1 IS
   PORT ( S, U, V, W : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
   M : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
END mux_2bit_3to1;

ARCHITECTURE Behavior OF mux_2bit_3to1 IS
BEGIN
	process (S, U, V, W)
	BEGIN
		if (S = "00") then
			M<= U;
		elsif (S = "01") then
			M<= V;
		elsif (S = "10") then
			M<= W;
		end if;
	end process;
END Behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char_7seg IS
	PORT (SW: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6));
END char_7seg;


ARCHITECTURE Behavior OF char_7seg IS

BEGIN
	process (SW)
	BEGIN
		if (SW= "00") then
			HEX0 <= "1000010";
		elsif (SW= "01") then
			HEX0 <= "0110000";
		elsif (SW= "10") then
			HEX0 <= "1001111";
		end if;
	end process;
END Behavior;
