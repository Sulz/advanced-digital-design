LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY lab3_pt2 IS
--uncomment next two lines
--  	PORT ( -- define input and output ports
--		   );
END lab3_pt2;

ARCHITECTURE Behavior OF lab3_pt2 IS
--	. . . declare signals
TYPE State_type IS (S1, S2, S3);
SIGNAL current_state, next_state : State_type; -- y_Q is present state, y_D is next state

  BEGIN
--
	PROCESS (reset, current_state) -- state table
	BEGIN
--		...
		case current_state IS
		WHEN S1 =>
			next_state <= S2;
  		   --...more....
		   -- if..then..else and so on
		WHEN S2 =>
			next_state <= S3
			
		when S3 =>
			next_state <= S1
			
   	    -- other states
END CASE;
END PROCESS; -- state table

PROCESS (Clock) -- state flip-flops
  BEGIN
 	-- . . .
  END PROCESS;
	-- . . . assignments for the LEDs and (maybe) outputs
END Behavior;
