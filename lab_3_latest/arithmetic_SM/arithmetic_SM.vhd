library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

entity arithmetic_SM is
	Port (  
			--Ai, Bi: in std_logic_vector(7 downto 0);
			data_in : in std_logic_vector(7 downto 0);
		   	--LdA, LdB, LdS, 
			selecter : in std_logic_vector(1 downto 0);
			
		   	Ci, clk : in std_logic;
			Co, Ovr: out std_logic;
			A_out, B_out, S_out : out std_logic_vector(6 downto 0);
			A_out2, B_out2 : out std_logic_vector(6 downto 0);
			S : out std_logic_vector(7 downto 0));
end arithmetic_SM;


architecture behavioral of arithmetic_SM is
	signal TA, TB, TS: std_logic_vector(7 downto 0);--define some intermediate signals
	signal TCo, TOvr: std_logic;	
	
begin
  --clocked_Stuff:
	process (clk) --clocked events ONLY
	begin
	  if (rising_edge(clk)) then --could use rising_edge function
	   if (selecter = "00") then
		TA <= data_in;
	   
		--add the rest of the clocked events in here
		
		elsif (selecter = "10") then
			TB <= data_in;
		
		elsif (selecter = "01") then
			Co <= TCo;
			Ovr <= TOvr;
			S <= TS;
		end if;
	   end if;
	end process Clocked_stuff;

--now add in the unclocked stuff here - i.e., the adder function
--this might either be a second process, or a bunch of concurrent statements --your choice

process (TA, TB, Ci)
	begin
		Ts <= TA + TB + Ci;
		--if Ai + Bi + Ci > "11111111" then
		if (Ts < TA and Ts < TB) then
			TCo <= '1';
			--Co <= (Ai and Bi) OR (Ci and (Ai or Bi));
		else TCo <= '0';
		end if;
		
		if (TA(7) = '1' and TB(7) = '1' and Ts(7) = '0') then
			TOvr <= '1';

		elsif (TA(7) = '0' and TB(7) = '0' and Ts(7) = '1') then
			TOvr <= '1';
			
		else TOvr <= '0';
		end if;
	end process random_label;
	
process(TA, TB, Ts)
begin

	if TA(7 downto 4) = "0001" then
		A_out2 <= "1111001";
	elsif TA(7 downto 4) = "0010" then
		A_out2 <= "0100100";
	elsif TA(7 downto 4) = "0011" then
		A_out2 <= "0110000";
	elsif TA(7 downto 4) = "0100" then
		A_out2 <= "0011001";
	elsif TA(7 downto 4) = "0101" then
		A_out2 <= "0010010";
	elsif TA(7 downto 4) = "0110" then
		A_out2 <= "0000010";
	elsif TA(7 downto 4) = "0111" then
		A_out2 <= "1111000";
	elsif TA(7 downto 4) = "1000" then
		A_out2 <= "0000000";
	elsif TA(7 downto 4) = "1001" then
		A_out2 <= "0010000";
	else A_out<="1111111";
	end if;
	
	if TA(3 downto 0) = "0001" then
		A_out <= "1111001";
	elsif TA(3 downto 0) = "0010" then
		A_out <= "0100100";
	elsif TA(3 downto 0) = "0011" then
		A_out <= "0110000";
	elsif TA(3 downto 0) = "0100" then
		A_out <= "0011001";
	elsif TA(3 downto 0) = "0101" then
		A_out <= "0010010";
	elsif TA(3 downto 0)  = "0110" then
		A_out <= "0000010";
	elsif TA(3 downto 0) = "0111" then
		A_out <= "1111000";
	elsif TA(3 downto 0) = "1000" then
		A_out <= "0000000";
	elsif TA(3 downto 0) = "1001" then
		A_out <= "0010000";
	else A_out<="1000000";
	end if;
	
	if TB(7 downto 4) = "0001" then
		B_out2 <= "1111001";
	elsif TB(7 downto 4) = "0010" then
		B_out2 <= "0100100";
	elsif TB(7 downto 4) = "0011" then
		B_out2 <= "0110000";
	elsif TB(7 downto 4) = "0100" then
		B_out2 <= "0011001";
	elsif TB(7 downto 4) = "0101" then
		B_out2 <= "0010010";
	elsif TB(7 downto 4) = "0110" then
		B_out2 <= "0000010";
	elsif TB(7 downto 4) = "0111" then
		B_out2 <= "1111000";
	elsif TB(7 downto 4) = "1000" then
		B_out2 <= "0000000";
	elsif TB(7 downto 4) = "1001" then
		B_out2 <= "0010000";
	else B_out2<="1000000";
	end if;
	
	if TB(3 downto 0) = "0001" then
		B_out <= "1111001";
	elsif TB(3 downto 0) = "0010" then
		B_out <= "0100100";
	elsif TB(3 downto 0) = "0011" then
		B_out <= "0110000";
	elsif TB(3 downto 0) = "0100" then
		B_out <= "0011001";
	elsif TB(3 downto 0) = "0101" then
		B_out <= "0010010";
	elsif TB(3 downto 0) = "0110" then
		B_out <= "0000010";
	elsif TB(3 downto 0) = "0111" then
		B_out <= "1111000";
	elsif TB(3 downto 0) = "1000" then
		B_out <= "0000000";
	elsif TB(3 downto 0) = "1001" then
		B_out <= "0010000";
	else B_out<="1000000";
	end if;

end process;

end behavioral;