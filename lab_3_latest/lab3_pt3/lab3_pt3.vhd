LIBRARY ieee;
USE ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

ENTITY lab3_pt3 IS
--uncomment next two lines
  	PORT ( -- define input and output ports
		   Ai, Bi : in std_logic_vector(7 downto 0);
		   -- LdA, LdB, LdS : in std_logic;
		   Clock : in std_logic;
		   reset : in std_logic;
		   S : out std_logic_vector (7 downto 0);
		   Op : out std_logic		   
		   );
END lab3_pt3;

ARCHITECTURE Behavior OF lab3_pt3 IS
--	. . . declare signals
TYPE State_type IS (S1, S2, S3);
SIGNAL current_state, next_state : State_type; -- y_Q is present state, y_D is next state
signal A_out, B_out, sum : std_logic_vector(7 downto 0);
signal LdA, LdB, LdS : std_logic;
		   
  BEGIN
--
	PROCESS (reset, current_state) -- state table
	BEGIN
--		...
		case current_state IS
		WHEN S1 =>
			next_state <= S2;
			LdA <= '1';
			LdB <= '0';
			LdS <= '0';
  		   --...more....
		   -- if..then..else and so on
		WHEN S2 =>
			LdA <= '0';
			LdB <= '1';
			LdS <= '0';
			next_state <= S3;
			
		when S3 =>
			LdA <= '0';
			LdB <= '0';
			LdS <= '1';
			next_state <= S1;
			
   	    -- other states
END CASE;
END PROCESS; -- state table

PROCESS (Clock) -- state flip-flops
  BEGIN
	if LdA = '1' then
		A_out <= Ai;
	end if;
	if LdB = '1' then
		B_out <= Bi;
	end if;
	if LdS = '1' then
		S <= std_logic_vector(unsigned(A_out) + unsigned(B_out));
	end if;
 	-- . . .
  END PROCESS;
	-- . . . assignments for the LEDs and (maybe) outputs
END Behavior;
