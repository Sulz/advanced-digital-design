library ieee;
use ieee.std_logic_1164.all;
-- example taken from P.P.Chu RTL Hardware Design using VHDL. 
-- (c) John Wiley

--this is the testbench entity.  Note that it sits over the top
--of the other code and has no ports at the entity level.
entity even_detector_testbench is
end even_detector_testbench;

architecture tb_arch of even_detector_testbench is

	component even_detector
			port 	(a: in std_logic_vector (2 downto 0) ;
	  				even: out std_logic);
	end component ;

signal test_in: std_logic_vector (2 downto 0) ;
signal test_out : std_logic;

begin
--instantiate the circuit under test
uut: even_detector
			port map(a=>test_in, even=>test_out); --NOTE named port mapping

--next the test vector generator process
process
begin
-- see that this comprises a series of set and wait statements
--this is a "cheap and cheerful" way to generate a set of test vectors
	test_in <= "000";
	wait for 200 ns;
	test_in <= "001";
	wait for 200 ns;
	test_in <= "010";
	wait for 200 ns;
	test_in <= "011";
	wait for 200 ns;
	test_in <= "100";
	wait for 200 ns;
	test_in <= "101";
	wait for 200 ns;
	test_in <= "110";
	wait for 200 ns;
	test_in <= "111";
	wait for 200 ns;
end process;

-- now the verifier process
process
--a local variable used to store the error status 
variable error_status : boolean;
begin
	--test_in is changed in the previous process
	--here, we are just sitting waiting for it to happen
	--remember that the processes are concurrent (but the code
	--inside the processes executes sequentially) 
	wait on test_in;
	wait for 100 ns;
	if ((test_in="000" and test_out = '1') or
		(test_in="001" and test_out = '0') or
		(test_in="010" and test_out = '0') or
		(test_in="011" and test_out = '1') or
		(test_in="100" and test_out = '0') or
		(test_in="101" and test_out = '1') or
		(test_in="110" and test_out = '1') or
		(test_in="111" and test_out = '0'))  then
	  
		error_status := false ;
	else
		error_status := true ;
	end if ;
--finally the error reporting
--in this example, there is only one assert statement.
--More usually, asserts are used throughout the testbench
--to verify the various parts of the circuit in turn 
	assert not error_status
	report " test failed."
	severity note ;
end process;

end tb_arch;