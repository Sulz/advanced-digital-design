library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

-- Top level entity
entity BCD_counter is
  Port (
      Clk : in std_logic;
      Rst : in std_logic;
      Count_unit : out std_logic_vector(3 downto 0);
      Count_ten : out std_logic_vector(3 downto 0);
      Count_hun : out std_logic_vector(3 downto 0)
      );
end BCD_counter;
	
architecture behav of BCD_counter is
	signal count_val_unit : std_logic_vector(3 downto 0);
	signal count_val_ten : std_logic_vector(3 downto 0);
	signal count_val_hun : std_logic_vector(3 downto 0);
	signal nxt_BCD : std_logic_vector(3 downto 0);
	signal custom_clk : std_logic;
	
begin
custom_clk <= not custom_clk after 1000000ns;
process (Clk, Rst)
begin
	if Rst = '1' then
		count_val_unit <= "0000";
		count_val_ten <= "0000";
		count_val_hun <= "0000";
		
	elsif (rising_edge(Clk)) then
		if custom_clk = '0' then
			if count_val_unit = "1001" then
				count_val_unit <= "0000";
				count_val_ten <= count_val_ten + 1;
				
				if count_val_ten = "1001" then
					count_val_ten <= "0000";
					count_val_hun <= count_val_hun + 1;
					
					if count_val_hun = "1001" then
						count_val_hun <= "0000";
					end if;
				end if;
				
			else
				count_val_unit <= count_val_unit + 1;
			end if;
		end if;
	end if;
	
end process;
-- 7Seg0 <= count_val_unit;
-- 7Seg1 <= count_val_ten;
-- 7Seg2 <= count_val_hun;
Count_unit <= count_val_unit;
Count_ten <= count_val_ten;
Count_hun <= count_val_hun;
end behav;

-- 7Seg mappings:
--0001 -> 11111001
--0010 -> 10100100
--0011 -> 10110000
--0100 -> 10011000
--0101 -> 10010010
--0110 -> 10000010
--0111 -> 11111000
--1000 -> 10000000
--1001 -> 10010000

