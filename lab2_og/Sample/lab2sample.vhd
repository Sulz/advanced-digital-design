library ieee;
use ieee.std_logic_1164.all;

entity lab2 is
	generic(counter_length : integer := 15);
	port(SW : in std_logic_vector (9 downto 0);
	HEX0, HEX1, HEX2 : out std_logic_vector (6 downto 0);
	CLOCK_50 : in std_logic;
	KEY : in std_logic_vector (3 downto 0));
end entity;

architecture arch of lab2 is
component ff_counter
generic(counter_length : integer);
port(enable, clk, clear_n : in std_logic;
	q : out std_logic_vector (counter_length downto 0));
end component;
signal q_out : std_logic_vector (counter_length downto 0);
begin
c0 : ff_counter generic map(counter_length => counter_length)
port map(enable => SW(1),
clear_n => SW(1),
clk => KEY(0),
q => q_out);

---hex decoder code missing-----


library ieee;
use ieee.std_logic_1164.all;

entity ff_counter is
generic(counter_length : integer);
port(enable, clk, clear_n : in std_logic;
	q : out std_logic_vector (counter_length downto 0));
end entity;

architecture arch of ff_counter is
component t_type_ff port(enable, clk, clear_n : in std_logic;
	q : out std_logic := '0');
end component;
signal q_vector, enable_vector : std_logic_vector (counter_length+1 downto 0) := (others => '0');
begin
t_ff: for i in 0 to counter_length generate
	tx : t_type_ff port map(enable => enable_vector(i),
	clk => clk,
	clear_n => clear_n,
	q => q_vector(i));
	enable_vector(i+1) <= q_vector(i) and enable_vector(i);
	q <= q_vector (counter_length downto 0);
	enable_vector(0) <= enable;
end generate;
end architecture;


library ieee;
use ieee.std_logic_1164.all;

entity t_type_ff is
port(enable, clk, clear_n : in std_logic;
	q : out std_logic := '0');
end entity;

architecture arch of t_type_ff is
signal q_n : std_logic := '1';
begin
	process(clk, clear_n)
	begin
		if clear_n = '0' then
			q <= '0';
			q_n <= '1';
		else
			if rising_edge(clk) and enable = '1' then
				q <= q_n;
				q_n <= not q_n;
			end if;
		end if;
	end process;
end architecture;
