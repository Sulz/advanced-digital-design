	LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY char_7seg IS
	PORT ( SW : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX1 : OUT STD_LOGIC_VECTOR(0 TO 6);
		HEX2 : OUT STD_LOGIC_VECTOR(0 TO 6));
END char_7seg;


ARCHITECTURE Behavior OF char_7seg IS

COMPONENT mux_2bit_3to1
	PORT ( S, U, V, W : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		M : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
END COMPONENT;

COMPONENT seg_7
	PORT ( M : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		HEX0  : OUT STD_LOGIC_VECTOR(0 TO 6));
END COMPONENT;

SIGNAL Ma, Mb, Mc : STD_LOGIC_VECTOR(1 DOWNTO 0);

BEGIN
	M0 : mux_2bit_3to1 PORT MAP (SW(9 DOWNTO 8), SW(5 DOWNTO 4), SW(3 DOWNTO 2),
	SW(1 DOWNTO 0), Ma);
	
	M1: mux_2bit_3to1 PORT MAP (SW(9 DOWNTO 8), SW(1 DOWNTO 0), SW(5 DOWNTO 4),
	SW(3 DOWNTO 2), Mb);
	
	M2: mux_2bit_3to1 PORT MAP (SW(9 DOWNTO 8), SW(3 DOWNTO 2), SW(1 DOWNTO 0),
	SW(5 DOWNTO 4), Mc); 
	
	H0: seg_7 PORT MAP (Ma, HEX0);
	H1: seg_7 PORT MAP (Mb, HEX1);
	H2: seg_7 PORT MAP (Mc, HEX2);
	
	
END Behavior;


LIBRARY ieee;
USE ieee.std_logic_1164.all;

 -- implement a 2-bit wide 3-to-1 multiplexer

ENTITY mux_2bit_3to1 IS
   PORT ( S, U, V, W : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
   M : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
END mux_2bit_3to1;

ARCHITECTURE Behavior OF mux_2bit_3to1 IS
BEGIN
	process (S, U, V, W)
	BEGIN
		if (S = "00") then
			M<= U;
		elsif (S = "01") then
			M<= V;
		elsif (S = "10") then
			M<= W;
		end if;
	end process;
END Behavior;

LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY seg_7 IS
	PORT (M: IN STD_LOGIC_VECTOR(1 DOWNTO 0);
		HEX0 : OUT STD_LOGIC_VECTOR(0 TO 6));
			
END seg_7;


ARCHITECTURE Behavior OF seg_7 IS

BEGIN
	process (M)
	BEGIN
		if (M= "00") then
			HEX0 <= "1000010";
		elsif (M= "01") then
			HEX0 <= "0110000";
		elsif (M= "10") then
			HEX0 <= "1001111";
		end if;
	end process;
END Behavior;
