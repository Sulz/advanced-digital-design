library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

-- Top level entity
entity BCD_counter is
  Port (
      
		SW : in std_logic_vector (9 downto 0);
		HEX0, HEX1, HEX2, HEX3 : out std_logic_vector (6 downto 0);
		CLOCK_50 : in std_logic;
		KEY : in std_logic_vector (3 downto 0);
		other_clk : out std_logic
		--LEDG: out std_logic_vector (7 DOWNTO 0) 

      );
      
end BCD_counter;
	
architecture behavior of BCD_counter is
	signal count_val_unit : std_logic_vector(3 downto 0);
	signal count_val_ten : std_logic_vector(3 downto 0);
	signal count_val_hun : std_logic_vector(3 downto 0);
	signal nxt_BCD : std_logic_vector(3 downto 0);
	signal custom_clk : std_logic;
	signal clk_count : integer;
	signal flag : std_logic;
	
begin

process (CLOCK_50)
begin
	if KEY = "1110" then
		count_val_unit <= "0000";
		count_val_ten <= "0000";
		count_val_hun <= "0000";
	end if;

	if rising_edge(CLOCK_50) then
		if (clk_count < 50000000) then
			clk_count <= clk_count + 1;
			flag <= '0';
		else
			clk_count <= 0;
			flag <= '1';
		end if;
	
	elsif (flag = '1') then
		if count_val_unit = "1001" then
			count_val_unit <= "0000";
			count_val_ten <= count_val_ten + 1; 
						
			if count_val_ten = "1001" then
				count_val_ten <= "0000";
				count_val_hun <= count_val_hun + 1;
				

				if count_val_hun = "1001" then
					count_val_hun <= "0000";
											
				end if;			
			end if;
									
		else
			count_val_unit <= count_val_unit + 1;
		end if;
		
	end if;			
--	end if;

	if count_val_unit = "0001" then
		HEX0 <= "1111001";
	elsif count_val_unit = "0010" then
		HEX0 <= "0100100";
	elsif count_val_unit = "0011" then
		HEX0 <= "0110000";
	elsif count_val_unit = "0100" then
		HEX0 <= "0011000";
	elsif count_val_unit = "0101" then
		HEX0 <= "0010010";
	elsif count_val_unit = "0110" then
		HEX0 <= "0000010";
	elsif count_val_unit = "0111" then
		HEX0 <= "1111000";
	elsif count_val_unit = "1000" then
		HEX0 <= "0000000";
	elsif count_val_unit = "1001" then
		HEX0 <= "0010000";
	else HEX0 <= "1111111";
	end if;
						
	if count_val_ten = "0001" then
		HEX1 <= "1111001";
	elsif count_val_ten = "0010" then
		HEX1 <= "0100100";
	elsif count_val_ten = "0011" then
		HEX1 <= "0110000";
	elsif count_val_ten = "0100" then
		HEX1 <= "0011000";
	elsif count_val_ten = "0101" then
		HEX1 <= "0010010";
	elsif count_val_ten = "0110" then
		HEX1 <= "0000010";
	elsif count_val_ten = "0111" then
		HEX1 <= "1111000";
	elsif count_val_ten = "1000" then
		HEX1 <= "0000000";
	elsif count_val_ten = "1001" then
		HEX1 <= "0010000";
	else HEX1 <= "1111111";
	end if;

	if count_val_hun = "0001" then
		HEX2 <= "1111001";
	elsif count_val_hun = "0010" then
		HEX2 <= "0100100";
	elsif count_val_hun = "0011" then
		HEX2 <= "0110000";
	elsif count_val_hun = "0100" then
		HEX2 <= "0011000";
	elsif count_val_hun = "0101" then
		HEX2 <= "0010010";
	elsif count_val_hun = "0110" then
		HEX2 <= "0000010";
	elsif count_val_hun = "0111" then
		HEX2 <= "1111000";
	elsif count_val_hun = "1000" then
		HEX2 <= "0000000";
	elsif count_val_hun = "1001" then
		HEX2 <= "0010000";
	else HEX2 <= "1111111";
	end if;
					
	HEX3 <= "1111111";
					
end process;	


custom_clk <= not custom_clk after 100ns;
other_clk <= custom_clk;

--LEDG(3) <= '1';

--Count_unit <= count_val_unit;
--Count_ten <= count_val_ten;
--Count_hun <= count_val_hun;

end behavior;
