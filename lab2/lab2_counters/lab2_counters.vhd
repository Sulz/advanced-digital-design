library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

entity lab2_counters is
	Port ( 	rst : in std_logic;
			clk : in std_logic;
			count: out std_logic_vector(15 downto 0));
end lab2_counters;


architecture behavioral of lab2_counters is
	signal temp: std_logic_vector(15 downto 0);
begin
	process (clk, rst)
	begin
	   if (rst = '1') then
		temp <= "0000000000000000";
	   elsif (clk'event and clk='1') then
		temp <= temp + 1;
	   end if;
	end process;
	count <= temp;
end behavioral;