library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries


-- Top level entity
entity structural_counter_onboard is
	generic(count_len : integer := 3);
  Port (
      
      En : in std_logic;
      
      Clk : in std_logic;
      --KEY : in std_logic_vector(3 downto 0);
      
      Rst : in std_logic;
      --SW : in std_logic_vector(9 downto 0);

	   -- test_OP : out std_logic_vector (3 downto 0);
      Count: out std_logic_vector(count_len downto 0);

      HEX0, HEX1, HEX2 : out std_logic_vector(6 downto 0));
  

end structural_counter_onboard;
	

-- Definition of interals of top level entity
architecture structure of structural_counter_onboard is

-- It has a flip flop component. With these mappings.
component T_Flop is
 generic(count_len : integer);
  port (en, clk, rst : in std_logic;
      count : out std_logic_vector (count_len downto 0));
 end component T_Flop;

component seg_decoder is
	generic(count_len : integer);
	port(
		--count : in std_logic_vector(count_len downto 0);
		hex_out : out std_logic_vector(6 downto 0)
		);
end component seg_decoder;
-- Each T_Flop output is a signal as it is not visible from outside the entity

 signal op : std_logic_vector(count_len downto 0);

-- Port mapping the T_Flop component

begin
  flop_0: T_Flop generic map(count_len => count_len)
    port map(
      en => En,
--      en => SW(1),
      
      clk => Clk,
      --clk => KEY(0),

      rst => Rst,
      --rst => SW(0),
      
      --count => Count
      count => op
    );
    
--decoder : seg_decoder generic map(count_len => count_len)
--	port map (
        --op => count,
--        hex_out => HEX0
--      );


process (op)

  begin
    if op = "0000" then
		-- op will not change.
     HEX0 <= "1111111";
    elsif op = "0001" then
    HEX0 <= "1111001";
    elsif op = "0010" then
    HEX0 <= "0100100";
    elsif op = "0011" then
    HEX0 <= "0110000";
    elsif op = "0100" then
    HEX0 <= "0011000";
    elsif op = "0101" then
    HEX0 <= "0010010";
    elsif op = "0110" then
    HEX0 <= "0000010";
    elsif op = "0111" then
    HEX0 <= "1111000";
    elsif op = "1000" then
    HEX0 <= "0000000";
    elsif op = "1001" then
    HEX0 <= "0010000";
    else HEX0 <= "1111111";
    end if;
end process;
--process (op)
--begin
--end process;

end architecture;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

--entity seg_decoder is
--	generic (count_len : integer);
--	port(
--		count : in std_logic_vector(count_len downto 0);
--		hex_out : out std_logic_vector(6 downto 0)
--	);
--end entity;

--architecture decode of seg_decoder is
--  begin

--end architecture;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries


-- Define the T_Flop component as an entity

-- Having already declared this component with these ports. 
--     Now instantiating/defining the actual component.
entity T_Flop is
generic(count_len : integer);
 port (en, clk, rst : in std_logic;
      count : out std_logic_vector (count_len downto 0));
end entity;


-- Define architecture of the entity
architecture flop_behav of T_Flop is
  component T_Flop_comp port(
          en, clk, rst : in std_logic;
          op : out std_logic
        );
  end component;

signal op_vector, en_vector : std_logic_vector (count_len + 1 downto 0) := (others => '0');
	
begin
  T_flop_part: for i in 0 to count_len generate
    actual_flop: T_Flop_comp port map
                  (
                    en => en_vector(i),
                    clk => clk,
                    rst => rst,
                    op => op_vector(i)
                  );
                  
          en_vector(i + 1) <= op_vector(i) and en_vector(i);
          count(count_len downto 0) <= op_vector(count_len downto 0);
          en_vector(0) <= en;
    end generate;
end architecture;


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries

entity T_Flop_comp is
  port (en, clk, rst : in std_logic;
        op : out std_logic);
end entity;

architecture arch of T_Flop_comp is

signal not_op : std_logic := '1';

begin
  process (clk, rst, en)
  begin
    if rst = '0' then
      op <= '0';
      not_op <= '1';
	else
		if rising_edge(clk) and en = '1' then
			op <= not_op;
			not_op <= not not_op;
		end if;
	end if;
  end process;
end architecture ; -- arch