library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries
use std.textio.all;

package REF_PACK is
	file INFILE : text is in "game_board.txt";
	file OUTFILE : text is out "curr_game.txt";
end REF_PACK;

library IEEE;
--use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries
use std.textio.all;

entity proj_rnd1 is
	Port(
		clk : in std_logic;
		fire : in std_logic;
		
		rows : out std_logic_vector(7 downto 0);
		--cols : out std_logic_vector(7 downto 0);
		
		x_coordinates : in std_logic_vector(2 downto 0);
		y_coordinates : in std_logic_vector(2 downto 0)
	);
end proj_rnd1;

architecture behavior of proj_rnd1 is

	signal test_row : std_logic_vector(7 downto 0);

	signal data_read : bit_vector(7 downto 0);
	
	signal EOF : bit:='0';
	
	type game_state is array(7 downto 0, 7 downto 0) of bit;
	signal curr_game_state : game_state;

	signal x_coord, y_coord : unsigned(0 to 2);
	
	signal curr_line : integer := 0;
	signal shot_fired : std_logic;

begin
	shot_fired <= fire;


READ_FILE: process
	variable in_line : line;
	variable read_var : bit_vector (7 downto 0);

	file game_file : text is in "game_board.txt";
		
begin
	wait until (rising_edge(clk));
	--if (rising_edge(clk)) then
		if (not endfile(game_file) and not EOF = '1') then
			readline(game_file, in_line);
			READ(in_line, read_var);
			data_read <= read_var;
			
			for i in 0 to 7 loop
				curr_game_state(curr_line, i) <= data_read(i);
				rows(i) <= '1';
				test_row(i) <= '1';
				
				--if data_read(i) = '1' then
					--test_row(i) <= '1';
				--elsif data_read(i) = '0' then
					--test_row(i) <= '0';
				--end if;
			end loop;
			rows <= test_row;
			
			
			curr_line <= curr_line + 1;
		else
			EOF <= '1';
			rows <= "11111111";
		end if;
	rows <= "11111111";
	
end process;

target_coordinates: process(shot_fired)
begin
	x_coord <= unsigned(x_coordinates);
	y_coord <= unsigned(y_coordinates);
	
end process;


end behavior;

