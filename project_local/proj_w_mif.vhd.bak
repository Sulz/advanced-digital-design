library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries
use ieee.numeric_std.all;

entity proj_w_mif is
  Port(
	-- Internal Clock
    clk : in std_logic;
    
    --Button press for firing at entered coordinate.
    fire : in std_logic;
        
    --rows : out std_logic;

	--Method for coordinates and reset.
    SW : in std_logic_vector (9 downto 0);
    
    -- The 4 7-Set displays
    HEX0 : out std_logic_vector (6 downto 0);
    HEX1 : out std_logic_vector (6 downto 0);
    HEX2 : out std_logic_vector (6 downto 0);
    HEX3 : out std_logic_vector (6 downto 0);
    
    -- All the green LED's
    LEDG : out std_logic_vector(7 downto 0);   
     
    -- Debug. Shows when a shot was a hit.
    random_var : out std_logic; 
    
    --Debug. Shows address given by incrementer module for attaining data at that address in MIF.
    address_tester : out std_logic_vector(5 downto 0);
    
    -- Debug. When loading bits from MIF. This mirrors the bit being loaded.
    bit_tester : out std_logic;
    
    --Debug. Was used to ensure new_game conditions recognized.
    new_game : out std_logic;
    
    -- Debug. Used to determine number of hits so far.
    hit_count : out std_logic_vector(3 downto 0);
    
    -- Debug. Used to trace whether or not the game has finished loading.
    has_loaded_check : out std_logic
  );
end proj_w_mif;


architecture behav of proj_w_mif is

-----------------------------------------------
--				Incrementer					 --
-----------------------------------------------

-- Iterates from 0-63. Giving address for MIF.
--		inc_clk : Clock for the component.
--  	game_over : When time to start new game. 
--					(Start loading from 0)
--  	output : Next address.
--  	loaded : When game board is loaded. Commence game.

component incrementer is
  port(
      inc_clk : in std_logic;
      game_over : in std_logic;
      output : out std_logic_vector (5 downto 0);
      loaded : out std_logic
    );
end component incrementer;

-----------------------------------------------
--			Memory Initialization		 	--
-----------------------------------------------

--		address : Where to get next bit of data from
--		clock : MIF clock.
--		q : the bit at address 'address'

component mega_mif is
  port(
      address : in std_logic_vector(5 downto 0);
      clock : in std_logic := '1';
      q : out std_logic 
    );
end component mega_mif;

--Damiens Components
component char_7seg
  port(   
    HEX : in std_logic_vector (3 downto 0);
    Display : out std_logic_vector (6 downto 0)
    );
end component;

 -- Not currently used
component Hit_LED is    
  Port (                   
    LEDR : out  STD_LOGIC:='0'
    );      
end component;
component Shot_LED is    
  Port (                   
    LEDG : out  STD_LOGIC:='0'
    );      
end component;
  
  --TODO: Retrace where new_game flags were used (hopefully I pushed it).
  
  --Sulz
  -- Defining the array which will hold the board.
  type game_status is array (63 downto 0) of std_logic;
  
  -- The current game board. '1' indicates presence of ship.
  signal game_state : game_status;
  
  
  --signal addr : std_logic_vector(5 downto 0);
  
  -- The bit from the MIF to place in array
  signal this_bit : std_logic;
  
  -- The output from the incrementer. CAN DELETE IF REPLACE WITH 'pointer' LINE 167
  signal add_ptr : std_logic_vector(5 downto 0);
  
  -- The pointer to pass to the MIF.
  signal pointer : std_logic_vector(5 downto 0);
  
  --signal num_hits : integer := 0;
  signal num_hits : std_logic_vector(3 downto 0);
  
  --May
  -- For converting the vector array of switches into coordinates.
  signal intX,intY : integer;
  
  -- To recognize a hit
  signal hit : std_logic;
  
  -- Convert the coordinates to an integer index.
  signal index : integer;
  
  --Damien
  -- For keeping track of the number of shots, hits, and for passing to HEX.
  signal hit_count_ones : std_logic_vector (3 downto 0) := "0000";
  signal hit_count_tens : std_logic_vector (3 downto 0) := "0000";
  signal shot_count_ones : std_logic_vector (3 downto 0) := "0000";
  signal shot_count_tens : std_logic_vector (3 downto 0) := "0000";

  -- To recognize when the gamestate has finished loading.
  signal has_loaded : std_logic;
  
  -- For updating the LED's (?) DEBUG
  signal LED_flash_miss : std_logic;
  
  -- Ingame array index used for updating the board on a hit.
  signal index_update : integer;
  
  -- Another signal for passing around on hit (perhaps replace with 'hit')
  signal hit_sig : std_logic;

  -- Reload the game when all the ships have been hit.
  signal end_this : std_logic;
  
  
  --signal new_game_flag : std_logic;
  
begin
-- The first component. The incrementer.
adresser : incrementer
  port map(
      inc_clk => clk,
      game_over => end_this,
      output => pointer,
      loaded => has_loaded
    );
--Debug. Tracing load.
has_loaded_check <= has_loaded;

-- Hold most recent address. *Delete*?
add_ptr <= pointer;

-- Second component. Memory Initialization File.
mif: mega_mif
  port map(
      address => add_ptr,
      clock => clk,
      q  => this_bit  
    );
    
--converting coordinates from std_logic_vector to integer
intX <= conv_integer(SW(2 downto 0)); 
intY <= conv_integer(SW(5 downto 3));

-- Converting from coordinates into an index.
index <= (8 * intY) + intX;

-- This process both loads and updates the 2D array.
--	Called when:
--		Powered on.
--		User hits a ship.
--		Once user hits all ships.
process(clk, add_ptr, has_loaded, hit_sig)
begin
  -- If the game has not loaded, keep loading. *RM POINTER?*
  if (has_loaded = '0' or pointer = "000000") then
    -- Put the recently attained bit from MIF into gamestate array.
    game_state(conv_integer(pointer)) <= this_bit;
    address_tester <= pointer; -- for debug
    bit_tester <= this_bit;		-- for debug
    
  elsif (has_loaded = '1' and hit_sig = '1') then
    -- Otherwise, we set the recently hit coordinate in the game to 0.
    game_state(index_update) <= '0';
  end if;
  
end process;
    
  -- Components for the 7-Seg decoders and display.
  s0 : char_7seg port map (HEX => hit_count_ones, Display => HEX0); 
  s1 : char_7seg port map (HEX => hit_count_tens, Display => HEX1);
  s2 : char_7seg port map (HEX => shot_count_ones, Display => HEX2);
  s3 : char_7seg port map (HEX => shot_count_tens, Display => HEX3);
 
process(fire)
  begin
  -- User lifts the button, and the game has loaded.
  if (falling_edge(fire) and has_loaded = '1') then
    -- check if there's a ship at the entered coordinates.
    if game_state(index) = '1' then 
      -- send a high for a hit  
      hit <= '1'; 
      -- Tell the system there was a hit.
      hit_sig <= '1';
      -- Reset hit. Required for correct operations.
      hit <= '0';
      -- Flash the low-acting LED's DEBUG
      LED_flash_miss <= '0';
      -- Whatever the current index is, update that coordinate in the game board.
      index_update <= index;
      
    else
      -- no hit
      hit <= '0'; 
      hit_sig <= '0';
      LED_flash_miss <= '1'; --DEBUG
  end if;
 end if;
 random_var <= hit_sig;

 -- If the switches are set for new game, or the game is loading.
 if (SW(9 downto 8)= "01" OR SW(9 downto 8)="11" or has_loaded = '0') then
  --Reset game score counts.
    hit_count_ones <= "0000"; 
    hit_count_tens <= "0000";
    shot_count_ones <= "0000"; 
    shot_count_tens <= "0000";
    --TODO - SET FOR NEW GAME!
    -- end_this <= '1';?
 
 -- If the switches are set for gameplay, and the game has loaded.
 elsif (SW(9 downto 8) = "00" and has_loaded = '1') then
   -- If the user has pressed the button.
   if rising_edge(fire) then -- <- This confuses me...
      -- If there was a hit.
      if hit_sig = '1' then
		-- Turn off the LED's.
        LEDG <= "00000000";
        
        -- Incremement the number of hits so far.
        hit_count_ones <= hit_count_ones + 1;
        if hit_count_ones = "1001" then
          hit_count_ones <= "0000";
          hit_count_tens <= hit_count_tens +1;
          if hit_count_tens = "1001" then
            hit_count_tens <= "0000";
          end if;
        end if;
        
        -- Increment number of shots so far.
        shot_count_ones <= shot_count_ones + 1;
        if shot_count_ones = "1001" then
          shot_count_ones <= "0000";
          shot_count_tens <= shot_count_tens +1;
          if shot_count_tens = "1001" then
            shot_count_tens <= "0000";
          end if;
        end if;

	-- If the shot was a miss.
      elsif hit_sig = '0' then
        -- Turn on the LED's.
        LEDG <= "11111111";
        -- Increment number of shots so far.
        shot_count_ones <= shot_count_ones + 1; 
        if shot_count_ones = "1001" then
          shot_count_ones <= "0000";
          shot_count_tens <= shot_count_tens +1;
          if shot_count_tens = "1001" then
            shot_count_tens <= "0000";
          end if;
        end if; 
      end if;       
    end if;
  end if; 
  
	hit_count <= hit_count_ones; --DEBUG
	
	-- Once the user hits the number of hittable components in the array, signal end of game.
	if (hit_count_ones = "0100" and hit_count_tens = "0000") then
--		new_game <= '1';
--		new_game_flag <= '1';

		-- End of game signal
		end_this <= '1';
		-- Reset the hit signal.
		hit_sig <= '0';
	else 
--		new_game <= '0';
--		new_game_flag <= '0';

		-- Not end of game yet.
		end_this <= '0';
	end if;   
end process;

end architecture;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL; -- Two very useful
use IEEE.STD_LOGIC_UNSIGNED.ALL; -- IEEE libraries


---------------------------
-- Incrementer Component --
---------------------------
entity incrementer is
  port(
      inc_clk : in std_logic;
      game_over : in std_logic;
      output : out std_logic_vector(5 downto 0);
      loaded : out std_logic
  );
end entity incrementer;

architecture behav of incrementer is
  signal address : std_logic_vector(5 downto 0):= "000000";
  signal load_check : std_logic := '0';

begin
process (inc_clk)
  begin
    if rising_edge(inc_clk) then
		-- Once we've gone through each address.
		if address = "111111" then
			-- Keep address constant (RM)?
			address <= address;
			-- Game has loaded.
			load_check <= '1';
		end if;
		
		-- If the game has played through.
		if game_over = '1' then
			-- Reset the address
			address <= "000000";
			-- Signal the game hasn't loaded.
			load_check <= '0';
		end if;
		
		-- While the game hasn't loaded (effectively a loop on each clock).
		if load_check = '0' then
			-- Increment the address.
			address <= address + 1;
		end if;
	     
  end if;
end process;
-- Return determined address.
output <= address;
-- Return loaded flag.
loaded <= load_check;
end architecture behav;


LIBRARY ieee;
USE ieee.std_logic_1164.all;
-- Generated by megafunction.
LIBRARY altera_mf;
USE altera_mf.all;

--------------------------------
-- Memory Initialization File --
--------------------------------
ENTITY mega_mif IS
  PORT
  (
    address   : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
    clock   : IN STD_LOGIC  := '1';
    q     : OUT STD_LOGIC
  );
END mega_mif;


ARCHITECTURE SYN OF mega_mif IS

  SIGNAL sub_wire0  : STD_LOGIC;

  COMPONENT altsyncram
  GENERIC (
    clock_enable_input_a    : STRING;
    clock_enable_output_a   : STRING;
    init_file   : STRING;
    intended_device_family    : STRING;
    lpm_hint    : STRING;
    lpm_type    : STRING;
    numwords_a    : NATURAL;
    operation_mode    : STRING;
    outdata_aclr_a    : STRING;
    outdata_reg_a   : STRING;
    widthad_a   : NATURAL;
    width_a   : NATURAL;
    width_byteena_a   : NATURAL
  );
  PORT (
      clock0  : IN STD_LOGIC ;
      address_a : IN STD_LOGIC_VECTOR (5 DOWNTO 0);
      q_a : OUT STD_LOGIC
  );
  END COMPONENT;

BEGIN
  q    <= sub_wire0;

  altsyncram_component : altsyncram
  GENERIC MAP (
    clock_enable_input_a => "BYPASS",
    clock_enable_output_a => "BYPASS",
    init_file => "proj_w_mif.mif",
    intended_device_family => "Cyclone II",
    lpm_hint => "ENABLE_RUNTIME_MOD=NO",
    lpm_type => "altsyncram",
    numwords_a => 64,
    operation_mode => "ROM",
    outdata_aclr_a => "NONE",
    outdata_reg_a => "CLOCK0",
    widthad_a => 6,
    width_a => 1,
    width_byteena_a => 1
  )
  
  PORT MAP (
    clock0 => clock,
    address_a => address,
    q_a => sub_wire0
  );

END SYN;

library ieee;
use ieee.std_logic_1164.all;

--------------------------------
-- 	   7 Segment Display 	  --
--------------------------------
entity char_7seg is 
  port(
    HEX : in std_logic_vector (3 downto 0); 
    Display : out std_logic_vector (6 downto 0)
    );
end entity;

-- Decode the provided BCD into 7Segment display drivers.
architecture behaviour of char_7seg is
  begin
    Process (HEX)
      begin
        case HEX is
          when "0000" => Display <= "1000000";--0
          when "0001" => Display <= "1111001";--1
          when "0010" => Display <= "0100100";--2
          when "0011" => Display <= "0110000";--3
          when "0100" => Display <= "0011001";--4
          when "0101" => Display <= "0010010";--5
          when "0110" => Display <= "0000010";--6
          when "0111" => Display <= "1111000";--7
          when "1000" => Display <= "0000000";--8
          when "1001" => Display <= "0010000";--9
          when others => Display <= "0000000";--0
        end case;   
    end Process;
end architecture;


--------------------------------
-- 	   	   Red LED's		  --
--------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
entity Hit_LED is    
  Port (                   
    LEDR : out  STD_LOGIC:='0'
    );
    
end Hit_LED;
 
architecture Behavioral of Hit_LED is
  begin
 
    LEDR <= '1';                   
 
end Behavioral;

--------------------------------
-- 	   	  Green LED's		  --
--------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
entity Shot_LED is    
  Port (                   
    LEDG : out  STD_LOGIC:='0'
    );
    
  end Shot_LED;
 
architecture Behavioral of Shot_LED is
  begin
 
    LEDG <= '1';                   
 
end Behavioral;